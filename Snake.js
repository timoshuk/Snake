var randomX = Math.round(Math.random()*30 + 5);
var randomY = Math.round(Math.random()*30 + 5);
var score = 0;
var snake = [[randomX,randomY],[randomX,randomY+1],[randomX,randomY+2]];
var direction = [1,0];
var canTurn = true;

function move() {
    snake.push(add(head(),direction));
    snake.shift();
    return snake
}

function eat() {
    snake.push(add(head(),direction));
    score++;
    return snake
}

function head() {
    return snake[snake.length-1]
}

function add(a,b) {
    return [ a[0] + b[0], a[1] + b[1] ]
}

const size = 10;
const xMax = 40;
const yMax = 40;

var ctx = picture.getContext('2d');
ctx.canvas.width = xMax*size;
ctx.canvas.height = yMax*size;

function drawSnake() {
    ctx.fillStyle = 'darkgreen';
    snake.forEach(
        function (s){
            ctx.beginPath();
            ctx.rect(s[0]*size,s[1]*size,size,size);
            ctx.fill();
        })
}

function updateWorld() {
    canTurn = true;
    ctx.clearRect(0, 0, xMax*size, yMax*size);
    if (equal(add(head(),direction), food)) {
        eat();
        newFood();
    }
    else
        move();
    if (bites() || hitsWall())
        endGame();
    drawSnake();
    drawFood();
}
window.setInterval(updateWorld,100);

window.addEventListener('keydown', check, false);
function check(e) {
    var code = e.keyCode;
    switch (code) {
        case 37: turn([-1,0]); canTurn = false; break;
        case 38: turn([0,-1]); canTurn = false; break;
        case 39: turn([1,0]);  canTurn = false; break;
        case 40: turn([0,1]);  canTurn = false; break;
    }
}

var food = [10,10];

function drawFood() {
    ctx.beginPath();
    ctx.fillStyle = "red";
    ctx.arc((food[0]+1/2)*size,
        (food[1]+1/2)*size, size/2, 0, 2*Math.PI);
    ctx.fill();
}

function newFood() {
    food = [ Math.round(Math.random()*40)
        , Math.round(Math.random()*40) ]
}
function equal(a, b) {
    return  a[0] == b[0] && a[1] == b[1];
}

function bites() {
    return tail().some(function(s){return equal(s, head())})
}

function turn(d) {
    if(!(direction[0]==d[0]||direction[1]==d[1]) && canTurn )
    direction = d
}

function tail() {
    return snake.slice(0,snake.length-1)
}

function endGame() {
    randomX = Math.round(Math.random()*30 + 5);
    randomY = Math.round(Math.random()*30 + 5);

    alert("Вы проиграли со счетом: " + score);
    snake = [[randomX,randomY],[randomX,randomY+1],[randomX,randomY+2]];
    direction = [1,0];
    food = [10, 10];
    score = 0;
}

function hitsWall() {
    return head()[0] == -1 || head()[0] == xMax || head()[1] == -1|| head()[1] == yMax;
}